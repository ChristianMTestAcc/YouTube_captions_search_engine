#!/usr/bin/python3

import os, requests, json

channelIds = next(os.walk('channels/'))[1]
maxResults = 50

channelIdsChunks = [channelIds[i : i + maxResults] for i in range(0, len(channelIds), maxResults)]
mostSubscriberCount = 0
mostSubscriberChannel = None

for channelIds in channelIdsChunks:
    url = 'https://yt.lemnoslife.com/noKey/channels?part=statistics&id=' + ','.join(channelIds)
    content = requests.get(url).text
    data = json.loads(content)
    items = data['items']
    for item in items:
        subscriberCount = int(item['statistics']['subscriberCount'])
        if mostSubscriberCount < subscriberCount:
            mostSubscriberCount = subscriberCount
            mostSubscriberChannel = item['id']

print(mostSubscriberChannel, mostSubscriberCount)

#!/usr/bin/python3

PREFIX = 'Comments per second: '
alreadyTreatedCommentsCount = 0

with open('nohup.out') as f:
    lines = f.read().splitlines()
    for line in lines:
        if PREFIX in line:
            alreadyTreatedCommentsCount += int(line.split(PREFIX)[-1])

print(alreadyTreatedCommentsCount)
